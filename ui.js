var ViewModel =
        function () {
    var that =
            this;

    this.uri =
            ko.observable(
                    "");

    this.uri.subscribe(
            function (value) {
        var uri = parse_uri(
                value);
        that.uriParsed(
                uri);
        that.varsParsed(
            _.map(
                parse_uri_query_vars(uri),
                function (
                    value,
                    key) {
                return {
                    name: key,
                    value: value
                }
            }));
    });

    this.varsParsed =
            ko.observableArray(
                    []);
 
    this.uriParsed =
            ko.observable(
                    {});

    this.gridViewModel =
            new ko.simpleGrid.viewModel({
                data: that.varsParsed,
                columns: [
                    {
                        headerText: "Name",
                        rowText: "name"
                    },
                    {
                        headerText: "Value",
                        rowText: "value"
                    }
                ]
            });
};

var myViewModel =
        new ViewModel();

ko.applyBindings(
        myViewModel);

myViewModel.uri(
        "http://www.google.com/service/mail?someVariable=someValue&someOtherVariable=someOtherValue#somefragment");
