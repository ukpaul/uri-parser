Implements RFC 3986 URI Parse

http://www.ietf.org/rfc/rfc3986.txt

Appendix B of RFC 3986 specifies a regular expression for parsing, this code
implements that with a user-interface attached. Browser JavaScript has a built
in parser for URIs but I used the specification RegEx parse instead so parsing
is not browser bound.

I chose JavaScript because of the ease of cross-platform GUI and lack of having
to manage a local environment.

Run Application: Open index.html

Run Tests: Open tests.html

See test comments for some further information.


Libraries

http://underscorejs.org/

For functional programming extensions. Useful for concise array/object
manipulation.

http://knockoutjs.com/ 

For simplifying dynamic JavaScript UIs with the Model-View-View Model (MVVM).
I used this for efficiency and making the implementation more like using a
desktop GUI library for more complex applications, like WPF or QT.