function parse_uri(
        uri) {
    var pattern = RegExp(
            "^(([^:/?#]+):)?(//([^/?#]*))?([^?#]*)(\\?([^#]*))?(#(.*))?");
    var matches = uri.match(
            pattern);
    return {
        scheme: matches[2],
        authority: matches[4],
        path: matches[5],
        query: matches[7],
        fragment: matches[9]
    };
}

function parse_uri_query_vars(uri) {
    return uri.query === undefined ? {} : _.object(
            _.map(uri.query.split("&"), function (element) {
        return element.split("=");
    }));
}
